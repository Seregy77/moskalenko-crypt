package com.moskalenko;

import java.util.Scanner;

public class ConsoleUserInterface implements UserInterface {
    private Scanner scanner = new Scanner(System.in);

    @Override
    public void print(String string) {
        System.out.println(string);
    }

    @Override
    public String readString() {
        return scanner.nextLine();
    }

    @Override
    public int readInt() {
        return scanner.nextInt();
    }
}

package com.moskalenko;

import com.moskalenko.cipher.CaesarShift;
import com.moskalenko.cipher.Cipher;
import com.moskalenko.cipher.TrithemiusShift;

public class UserInterfaceDecrypter implements Decrypter {
    private UserInterface userInterface;

    public UserInterfaceDecrypter(UserInterface userInterface) {
        this.userInterface = userInterface;
    }

    @Override
    public String decrypt(String input){
        if (input.isEmpty()) {
            throw new IllegalArgumentException("Input string can't be empty");
        }

        StringBuilder sb = new StringBuilder("Choose cipher to decrypt your text:");
        for (Cipher cipher : Cipher.values()) {
            sb.append(System.lineSeparator())
                    .append(cipher.getValue()).append(". ")
                    .append(cipher.getDescription());
        }
        userInterface.print(sb.toString());

        int choice = userInterface.readInt();

        if (choice == Cipher.CAESAR.getValue()) {
            return decryptWithCaesar(input);
        } else if (choice == Cipher.TRITHEMIUS.getValue()) {
            return decryptWithTrithemius(input);
        } else if (choice == Cipher.XOR.getValue()) {
            return decryptWithXOR(input);
        } else {
            userInterface.print("You have to enter a valid number to make a choice");
            return decrypt(input);
        }
    }

    private String decryptWithCaesar(String input) {
        StringBuilder sb = new StringBuilder("Choose shift value:");
        for (CaesarShift shift : CaesarShift.values()) {
            sb.append(System.lineSeparator())
                    .append(shift.getValue()).append(". ")
                    .append((shift.getShift() > 0) ? "+" : "")
                    .append(shift.getShift());
        }
        userInterface.print(sb.toString());

        CaesarShift choice = CaesarShift.valueOf(userInterface.readInt());

        StringBuilder decrypted = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            decrypted.append(Utils.shiftLetter(input.charAt(i), -choice.getShift()));
        }

        return decrypted.toString();
    }

    private String decryptWithTrithemius(String input) {
        StringBuilder sb = new StringBuilder("Choose shift value:");
        for (TrithemiusShift shift : TrithemiusShift.values()) {
            sb.append(System.lineSeparator())
                    .append(shift.getValue()).append(". ")
                    .append(shift.getDescription());
        }
        userInterface.print(sb.toString());

        TrithemiusShift choice = TrithemiusShift.valueOf(userInterface.readInt());

        StringBuilder decrypted = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            switch (choice) {
                case CONSTANT:
                    decrypted.append(Utils.shiftLetter(input.charAt(i), -choice.getShift()));
                    break;
                case VARIABLE_PLUS_THREE:
                    decrypted.append(Utils.shiftLetter(input.charAt(i), -(i + choice.getShift())));
                    break;
                case VARIABLE_SQUARED_PLUS_THREE:
                    decrypted.append(Utils.shiftLetter(input.charAt(i), -(i*i + choice.getShift())));
                    break;
                default:
                    decrypted.append(Utils.shiftLetter(input.charAt(i), 0));
            }
        }

        return decrypted.toString();
    }

    private String decryptWithXOR(String input) {
        userInterface.readString();
        userInterface.print("Enter key:");
        String key = userInterface.readString();
        if (key.isEmpty()) {
            userInterface.print("Key can't be empty");
            return decryptWithXOR(input);
        }

        StringBuilder decrypted = new StringBuilder();
        int k = 0;
        for (int i = 0; i < input.length(); i++) {
            decrypted.append(Utils.shiftLetter(input.charAt(i), key.charAt(k++), false));

            if (k >= key.length()) {
                k = 0;
            }
        }
        return decrypted.toString();
    }
}

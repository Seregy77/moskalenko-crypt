package com.moskalenko.cipher;

public enum CaesarShift {
    PLUS_ONE(1, +1),
    PLUS_THREE(2, +3),
    PLUS_FIVE(3, +5),
    MINUS_ONE(4, -1),
    MINUS_THREE(5, -3),
    MINUS_FIVE(6, -5),
    ZERO(7, 0);

    private int id;
    private int shift;

    CaesarShift(int id, int shift) {
        this.id = id;
        this.shift = shift;
    }

    public int getValue() {
        return id;
    }

    public int getShift() {
        return shift;
    }

    public static CaesarShift valueOf(int id) {
        switch (id) {
            case 1:
                return PLUS_ONE;
            case 2:
                return PLUS_THREE;
            case 3:
                return PLUS_FIVE;
            case 4:
                return MINUS_ONE;
            case 5:
                return MINUS_THREE;
            case 6:
                return MINUS_FIVE;
            default:
                return ZERO;
        }
    }
}

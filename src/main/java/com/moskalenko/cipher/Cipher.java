package com.moskalenko.cipher;

public enum Cipher {
    CAESAR(1, "Caesar cipher"),
    TRITHEMIUS(2, "Trithemius cipther"),
    XOR(3, "XOR cipher");

    private int id;
    private String description;

    Cipher(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getValue() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}

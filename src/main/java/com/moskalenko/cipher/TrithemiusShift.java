package com.moskalenko.cipher;

public enum TrithemiusShift {
    CONSTANT(1, +3, "+ 3"),
    VARIABLE_PLUS_THREE(2, +3, "x + 3"),
    VARIABLE_SQUARED_PLUS_THREE(3, +3, "x^2 + 3"),
    ZERO(7, 0, "0");

    private int id;
    private int shift;
    private String description;

    TrithemiusShift(int id, int shift, String description) {
        this.id = id;
        this.shift = shift;
        this.description = description;
    }

    public int getValue() {
        return id;
    }

    public int getShift() {
        return shift;
    }

    public String getDescription() {
        return description;
    }

    public static TrithemiusShift valueOf(int id) {
        switch (id) {
            case 1:
                return CONSTANT;
            case 2:
                return VARIABLE_PLUS_THREE;
            case 3:
                return VARIABLE_SQUARED_PLUS_THREE;
            default:
                return ZERO;
        }
    }
}

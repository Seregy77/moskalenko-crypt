package com.moskalenko;

public interface Decrypter {
    String decrypt(String input);
}

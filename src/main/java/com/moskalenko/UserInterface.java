package com.moskalenko;

public interface UserInterface {
    void print(String string);
    String readString();
    int readInt();
}

package com.moskalenko;

public interface Encrypter {
    String encrypt(String input);
}

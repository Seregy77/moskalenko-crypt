package com.moskalenko;

public class Utils {
    public static char shiftLetter(char letter, int shift) {
        final int alphaLength = 26;
        final char asciiShift = Character.isUpperCase(letter) ? 'A' : 'a';
        final int cipherShift = shift % alphaLength;

        // shift down to 0..25 for a..z
        char shifted = (char) (letter - asciiShift);

        if (shifted > alphaLength - 1) {
            return letter;
        }

        // rotate the letter and handle "wrap-around" for negatives and value >= 26
        shifted = (char) ((shifted + cipherShift + alphaLength) % alphaLength);
        // shift back up to english characters
        return (char) (shifted + asciiShift);
    }

    public static char shiftLetter(char letter, char shiftCharacter, boolean shiftRight) {
        final int alphaLength = 26;
        final char asciiShift = Character.isUpperCase(shiftCharacter) ? 'A' : 'a';
        int shift = (char) (shiftCharacter - asciiShift);

        if (shift >= alphaLength - 1) {
            shift = 0;
        }

        if (!shiftRight) {
            shift = -shift;
        }

        return shiftLetter(letter, shift);
    }
}

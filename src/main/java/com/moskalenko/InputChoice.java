package com.moskalenko;

public enum InputChoice {
    Encrypt(1),
    Decrypt(2);

    private int id;

    InputChoice(int id) {
        this.id = id;
    }

    public int getValue() {
        return id;
    }
}

package com.moskalenko;

import java.util.Scanner;

public class App {
    private Encrypter encrypter;
    private Decrypter decrypter;
    private UserInterface userInterface;

    public App(Encrypter encrypter, Decrypter decrypter, UserInterface userInterface) {
        this.encrypter = encrypter;
        this.decrypter = decrypter;
        this.userInterface = userInterface;
    }

    public void start() {
        userInterface.print(String.format("Welcome to the console encryption/decryption application." +
                        "%sEnter %d to encrypt text %sEnter %d to decrypt text%s",
                System.lineSeparator(), InputChoice.Encrypt.getValue(),
                System.lineSeparator(), InputChoice.Decrypt.getValue(),
                System.lineSeparator()));
        int choice = userInterface.readInt();

        String textPrompt = "Enter the text you wish to ";
        if (choice == InputChoice.Encrypt.getValue()) {
            textPrompt += "encrypt:";
        } else if (choice == InputChoice.Decrypt.getValue()) {
            textPrompt += "decrypt:";
        } else {
            userInterface.print("You have to enter a valid number to make a choice");
            start();
            return;
        }

        userInterface.print(textPrompt);
        String text = userInterface.readString();
        if (choice == InputChoice.Encrypt.getValue()) {
            userInterface.print("Encrypted text: ");
            userInterface.print(encrypter.encrypt(text));
        } else if (choice == InputChoice.Decrypt.getValue()) {
            userInterface.print("Decrypted text: ");
            userInterface.print(decrypter.decrypt(text));
        }
    }

    public static void main(String[] args) {
        UserInterface userInterface = new ConsoleUserInterface();
        Encrypter encrypter = new UserInterfaceEncrypter(userInterface);
        Decrypter decrypter = new UserInterfaceDecrypter(userInterface);
        App app = new App(encrypter, decrypter, userInterface);
        app.start();
    }
}

package com.moskalenko;

import com.moskalenko.cipher.CaesarShift;
import com.moskalenko.cipher.Cipher;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AppTest {

    @Test
    public void testEncryption() {
        UserInterface mockedUI = mock(UserInterface.class);
        when(mockedUI.readString()).thenReturn("Secret text");
        when(mockedUI.readInt())
                .thenReturn(InputChoice.Encrypt.getValue())
                .thenReturn(Cipher.CAESAR.getValue())
                .thenReturn(CaesarShift.PLUS_THREE.getValue());
        Decrypter mockedDecrypter = mock(Decrypter.class);
        Encrypter encrypter = new UserInterfaceEncrypter(mockedUI);
        App app = new App(encrypter, mockedDecrypter, mockedUI);

        app.start();
        verify(mockedUI).print("Vhfuhw whaw");
    }

    @Test
    public void testDecryption() {
        UserInterface mockedUI = mock(UserInterface.class);
        when(mockedUI.readString()).thenReturn("Xzujw xjhwjy yjcy");
        when(mockedUI.readInt())
                .thenReturn(InputChoice.Decrypt.getValue())
                .thenReturn(Cipher.CAESAR.getValue())
                .thenReturn(CaesarShift.PLUS_FIVE.getValue());
        Decrypter decrypter = new UserInterfaceDecrypter(mockedUI);
        Encrypter mockedEncrypter = mock(Encrypter.class);
        App app = new App(mockedEncrypter, decrypter, mockedUI);

        app.start();
        verify(mockedUI).print("Super secret text");
    }
}